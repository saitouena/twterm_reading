* tweetbox.rb
たぶんツイートする際にバリデーションしたりするクラス。
#+BEGIN_SRC 
./lib/twterm/app.rb:      @tweetbox = Tweetbox.new(self, client)
#+END_SRC
** ask_and_post
プロンプトを表示して投稿するんかな？確信がないわ。
** ask
ここのメイン。あたらしいスレッドを作ってそこで動く。postprocessorがなんのクラスなんだろう。これもバリデーションするひとっぽいな。validate!を読んでいる。app.register_interruption_handlerはスレッドを殺すときのハンドラなんだろう。app.rbに追記しておくべきか。thred.joinがわからん tweetbox.rbのaskはけっこう複雑な動きをしていて、送信されるのはraw_textで、textはバリデーションに使われるだけg。textを変更してもなにも変わらないので注意。
** validate!
*** Twitter::TwitterText::Validation.parse_tweet
https://github.com/twitter/twitter-text/tree/master/rb これのLength Validationのところに書いてある。長さでバリデーションしてるだけ。文字セット？によって重みが違うっぽいな。そこらへんはツイッターの文字数制限の仕様をしらべるしかない。
** compose
ここのメッセージを書き換えてもmessageが変化しないのはなんでだろう。ask_and_postに原因が...?
** ask_and_post
よくわかんない. 
